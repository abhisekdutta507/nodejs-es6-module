import { Router } from 'express';

const router = Router();

router.get('/test', (req, res) => {
    return res.send({
        status: 'Work in progress...'
    });
});

export default router;

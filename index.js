import express from 'express';
import router from './router.js';
const app = express();

app.use('/api', router);

app.listen(process.env.PORT || 5000, () => {
    console.log(`Node.js application is hosted on http://localhost:5000/api`);
});
